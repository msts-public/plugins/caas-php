<?php
declare(strict_types=1);

// phpcs:ignore
require __DIR__ . '/../vendor/autoload.php';

try {
    $caaS = new Msts\CaaS\CaaS('API-KEY');
    $webhookList = $caaS->webhooks->list();
    // phpcs:ignore
    var_dump($webhookList);
} catch (Msts\CaaS\Exception\ApiClientException $e) {
    // phpcs:ignore
    var_dump($e->getMessage());
}
