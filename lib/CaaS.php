<?php
declare(strict_types=1);

namespace Msts\CaaS;

use GuzzleHttp\Client;
use Msts\CaaS\Model\Buyer\BuyerApiCall;
use Msts\CaaS\Model\Charge\ChargeApiCall;
use Msts\CaaS\Model\Charge\MapCharge;
use Msts\CaaS\Model\Preauthorization\PreauthorizationApiCall;
use Msts\CaaS\Model\Webhook\WebhookApiCall;
use Msts\CaaS\Model\Credit\CreditApiCall;
use Msts\CaaS\Model\Credit\MapCredit;

class CaaS
{
    /**
     * @var BuyerApiCall
     */
    public $buyer;

    /**
     * @var ChargeApiCall
     */
    public $charge;

    /**
     * @var PreauthorizationApiCall
     */
    public $preauthorization;

    /**
     * @var WebhookApiCall
     */
    public $webhooks;

    /**
     * @var CreditApiCall
     */
    public $credit;

    /**
     * @var CaaSOptions
     */
    private $options;

    public function __construct(string $apiKey, ?CaaSOptions $options = null)
    {
        $this->options = $options ?? new CaaSOptions();
        $apiClient = new ApiClient($this->options->getLogger(), $this->options->getMaskValue(), new Client());
        $mstsRequest = $this->options->getMstsRequest();
        $configProvider = $mstsRequest->getConfigProvider();
        $configProvider->setApiKey($apiKey);
        $this->buyer = new BuyerApiCall(
            $apiClient,
            $mstsRequest
        );
        $this->charge = new ChargeApiCall(
            $apiClient,
            new MapCharge(),
            $mstsRequest
        );
        $this->preauthorization = new PreauthorizationApiCall(
            $apiClient,
            $mstsRequest
        );
        $this->webhooks = new WebhookApiCall(
            $apiClient,
            $mstsRequest
        );
        $this->credit = new CreditApiCall(
            $apiClient,
            new MapCredit(),
            $mstsRequest
        );
    }
}
