<?php

declare(strict_types=1);

namespace Msts\CaaS\Api\Data\Credit;

use Msts\CaaS\Api\Data\Credit\CreditDetailInterface;

interface CreditResponseInterface
{
    /**
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * @param string $id
     * @return CreditResponseInterface
     */
    public function setId(string $id): CreditResponseInterface;

    /**
     * @return string|null
     */
    public function getChargeId(): ?string;

    /**
     * @param string $chargeId
     * @return CreditResponseInterface
     */
    public function setChargeId(string $chargeId): CreditResponseInterface;

    /**
     * @return string|null
     */
    public function getSellerId(): ?string;

    /**
     * @param string $sellerId
     * @return CreditResponseInterface
     */
    public function setSellerId(string $sellerId): CreditResponseInterface;

    /**
     * @return string|null
     */
    public function getBuyerId(): ?string;

    /**
     * @param string $buyerId
     * @return CreditResponseInterface
     */
    public function setBuyerId(string $buyerId): CreditResponseInterface;

    /**
     * @return string|null
     */
    public function getStatus(): ?string;

    /**
     * @param string $status
     * @return CreditResponseInterface
     */
    public function setStatus(string $status): CreditResponseInterface;

    /**
     * @return string|null
     */
    public function getCurrency(): ?string;

    /**
     * @param string $currency
     * @return CreditResponseInterface
     */
    public function setCurrency(string $currency): CreditResponseInterface;

    /**
     * @return int|null
     */
    public function getTotalAmount(): ?int;

    /**
     * @param int $totalAmount
     * @return CreditResponseInterface
     */
    public function setTotalAmount(int $totalAmount): CreditResponseInterface;

    /**
     * @return int|null
     */
    public function getTaxAmount(): ?int;

    /**
     * @param int $taxAmount
     * @return CreditResponseInterface
     */
    public function setTaxAmount(int $taxAmount): CreditResponseInterface;

    /**
     * @return int|null
     */
    public function getShippingAmount(): ?int;

    /**
     * @param int $shippingAmount
     * @return CreditResponseInterface
     */
    public function setShippingAmount(int $shippingAmount): CreditResponseInterface;

    /**
     * @return int|null
     */
    public function getShippingTaxAmount(): ?int;

    /**
     * @param int $shippingTaxAmount
     * @return CreditResponseInterface
     */
    public function setShippingTaxAmount(int $shippingTaxAmount): CreditResponseInterface;

    /**
     * @return int|null
     */
    public function getShippingDiscountAmount(): ?int;

    /**
     * @param int $shippingDiscountAmount
     * @return CreditResponseInterface
     */
    public function setShippingDiscountAmount(int $shippingDiscountAmount): CreditResponseInterface;

    /**
     * @return int|null
     */
    public function getDiscountAmount(): ?int;

    /**
     * @param int $discountAmount
     * @return CreditResponseInterface
     */
    public function setDiscountAmount(int $discountAmount): CreditResponseInterface;

    /**
     * @return string|null
     */
    public function getCreditAmountCurrency(): ?string;

    /**
     * @param string $creditAmountCurrency
     * @return CreditResponseInterface
     */
    public function setCreditAmountCurrency(string $creditAmountCurrency): CreditResponseInterface;

    /**
     * @return int|null
     */
    public function getCreditAmount(): ?int;

    /**
     * @param int $creditAmount
     * @return CreditResponseInterface
     */
    public function setCreditAmount(int $creditAmount): CreditResponseInterface;

    /**
     * @return string|null
     */
    public function getCreditReason(): ?string;

    /**
     * @param string $creditReason
     * @return CreditResponseInterface
     */
    public function setCreditReason(string $creditReason): CreditResponseInterface;

    /**
     * @return string|null
     */
    public function getCreditComment(): ?string;

    /**
     * @param string $creditComment
     * @return CreditResponseInterface
     */
    public function setCreditComment(string $creditComment): CreditResponseInterface;

    /**
     * @return CreditDetailInterface[]
     */
    public function getDetails(): ?array;

    /**
     * @param array $items
     * @return CreditResponseInterface
     */
    public function setDetails(array $items): CreditResponseInterface;

    /**
     * @return CreditDetailInterface[]
     */
    public function getShippingTaxDetails(): ?array;

    /**
     * @param array $items
     * @return CreditResponseInterface
     */
    public function setShippingTaxDetails(array $items): CreditResponseInterface;
}
