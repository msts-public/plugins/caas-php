<?php

declare(strict_types=1);

namespace Msts\CaaS\Api\Data\Credit\ListMethod;

interface ListCreditsRequestInterface
{
    /**
     * @return int|null
     */
    public function getChargeId(): ?int;

    /**
     * @param int $pageSize
     * @return ListCreditsRequestInterface
     */
    public function setChargeId(int $pageSize): ListCreditsRequestInterface;

    /**
     * @return int|null
     */
    public function getPageSize(): ?int;

    /**
     * @param int $pageSize
     * @return ListCreditsRequestInterface
     */
    public function setPageSize(int $pageSize): ListCreditsRequestInterface;

    /**
     * @return int|null
     */
    public function getPageNumber(): ?int;

    /**
     * @param int $pageNumber
     * @return ListCreditsRequestInterface
     */
    public function setPageNumber(int $pageNumber): ListCreditsRequestInterface;

    /**
     * @return string|null
     */
    public function getFromData(): ?string;

    /**
     * @param string $fromDate
     * @return ListCreditsRequestInterface
     */
    public function setFromData(string $fromDate): ListCreditsRequestInterface;

    /**
     * @return string|null
     */
    public function getToData(): ?string;

    /**
     * @param string $toData
     * @return ListCreditsRequestInterface
     */
    public function setToData(string $toData): ListCreditsRequestInterface;

    /**
     * @return array
     */
    public function getRequestData(): array;
}
