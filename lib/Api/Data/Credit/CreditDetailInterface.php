<?php

declare(strict_types=1);

namespace Msts\CaaS\Api\Data\Credit;

interface CreditDetailInterface
{
  /**
   * @return string|null
   */
  public function getSku(): ?string;

  /**
   * @param string $sku
   * @return CreditDetailInterface
   */
  public function setSku(string $sku): CreditDetailInterface;

  /**
   * @return string|null
   */
  public function getDescription(): ?string;

  /**
   * @param string $description
   * @return CreditDetailInterface
   */
  public function setDescription(string $description): CreditDetailInterface;

  /**
   * @return float|null
   */
  public function getQuantity(): ?float;

  /**
   * @param float $quantity
   * @return CreditDetailInterface
   */
  public function setQuantity(float $quantity): CreditDetailInterface;

  /**
   * @return int|null
   */
  public function getUnitPrice(): ?int;

  /**
   * @param int $unitPrice
   * @return CreditDetailInterface
   */
  public function setUnitPrice(int $unitPrice): CreditDetailInterface;

  /**
   * @return int|null
   */
  public function getTaxAmount(): ?int;

  /**
   * @param int $taxAmount
   * @return CreditDetailInterface
   */
  public function setTaxAmount(int $taxAmount): CreditDetailInterface;

  /**
   * @return int|null
   */
  public function getDiscountAmount(): ?int;

  /**
   * @param int $discountAmount
   * @return CreditDetailInterface
   */
  public function setDiscountAmount(int $discountAmount): CreditDetailInterface;

  /**
   * @return int|null
   */
  public function getSubtotal(): ?int;

  /**
   * @param int $subtotal
   * @return CreditDetailInterface
   */
  public function setSubtotal(int $subtotal): CreditDetailInterface;

  /**
   * @return array
   */
  public function getTaxDetails(): array;

  /**
   * @param array $taxDetails
   * @return CreditDetailInterface
   */
  public function setTaxDetails(array $taxDetails): CreditDetailInterface;

  /**
   * @return array
   */
  public function getRequestData(): array;
}
