<?php

declare(strict_types=1);

namespace Msts\CaaS\Api\Data\Credit\CreateMethod;

use Msts\CaaS\Api\Data\Credit\CreditResponseInterface;

interface CreateCreditRequestInterface extends CreditResponseInterface
{
  /**
   * @param string $chargeId
   * @return CreditResponseInterface
   */
  public function setChargeId(string $chargeId): CreditResponseInterface;

  /**
   * @param int $totalAmount
   * @return CreditResponseInterface
   */
  public function setTotalAmount(int $totalAmount): CreditResponseInterface;

  /**
   * @param int $taxAmount
   * @return CreditResponseInterface
   */
  public function setTaxAmount(int $taxAmount): CreditResponseInterface;

  /**
   * @param int $shippingAmount
   * @return CreditResponseInterface
   */
  public function setShippingAmount(int $shippingAmount): CreditResponseInterface;

  /**
   * @param int $shippingTaxAmount
   * @return CreditResponseInterface
   */
  public function setShippingTaxAmount(int $shippingTaxAmount): CreditResponseInterface;

  /**
   * @param int $shippingDiscountAmount
   * @return CreditResponseInterface
   */
  public function setShippingDiscountAmount(int $shippingDiscountAmount): CreditResponseInterface;

  /**
   * @param int $discountAmount
   * @return CreditResponseInterface
   */
  public function setDiscountAmount(int $discountAmount): CreditResponseInterface;

  /**
   * @param string $creditReason
   * @return CreditResponseInterface
   */
  public function setCreditReason(string $creditReason): CreditResponseInterface;

  /**
   * @param string $creditComment
   * @return CreditResponseInterface
   */
  public function setCreditComment(string $creditComment): CreditResponseInterface;

  /**
   * @param array $items
   * @return CreditResponseInterface
   */
  public function setDetails(array $items): CreditResponseInterface;

  /**
   * @param array $items
   * @return CreditResponseInterface
   */
  public function setShippingTaxDetails(array $items): CreditResponseInterface;

  /**
   * @return array
   */
  public function getRequestData(): array;
}
