<?php
declare(strict_types=1);

namespace Msts\CaaS\Api\Data\Preauthorization\UpdateMethod;

interface UpdateAPreauthorizationRequestInterface
{
    /**
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * @param string $id
     * @return UpdateAPreauthorizationRequestInterface
     */
    public function setId(string $id): UpdateAPreauthorizationRequestInterface;

    /**
     * @return string|null
     */
    public function getPreauthorizedAmount(): ?string;

    /**
     * @param int $preauthorizedAmount
     * @return UpdateAPreauthorizationRequestInterface
     */
    public function setPreauthorizedAmount(int $preauthorizedAmount): UpdateAPreauthorizationRequestInterface;

    /**
     * @return array
     */
    public function getRequestData(): array;
}
