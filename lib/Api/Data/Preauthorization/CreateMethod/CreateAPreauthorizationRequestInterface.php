<?php
declare(strict_types=1);

namespace Msts\CaaS\Api\Data\Preauthorization\CreateMethod;

interface CreateAPreauthorizationRequestInterface
{
    /**
     * @return string|null
     */
    public function getSellerId(): ?string;

    /**
     * @param string $sellerId
     * @return CreateAPreauthorizationRequestInterface
     */
    public function setSellerId(string $sellerId): CreateAPreauthorizationRequestInterface;

    /**
     * @return string|null
     */
    public function getBuyerId(): ?string;

    /**
     * @param string $buyerId
     * @return CreateAPreauthorizationRequestInterface
     */
    public function setBuyerId(string $buyerId): CreateAPreauthorizationRequestInterface;

    /**
     * @return string|null
     */
    public function getCurrency(): ?string;

    /**
     * @param string $currency
     * @return CreateAPreauthorizationRequestInterface
     */
    public function setCurrency(string $currency): CreateAPreauthorizationRequestInterface;

    /**
     * @return int|null
     */
    public function getPreauthorizedAmount(): ?int;

    /**
     * @param int $preauthorizedAmount
     * @return CreateAPreauthorizationRequestInterface
     */
    public function setPreauthorizedAmount(int $preauthorizedAmount): CreateAPreauthorizationRequestInterface;

    /**
     * @return string|null
     */
    public function getPoNumber(): ?string;

    /**
     * @param string $poNumber
     * @return CreateAPreauthorizationRequestInterface
     */
    public function setPoNumber(string $poNumber): CreateAPreauthorizationRequestInterface;

    /**
     * @return array
     */
    public function getRequestData(): array;
}
