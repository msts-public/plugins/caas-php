<?php
declare(strict_types=1);

namespace Msts\CaaS\Api\Data\Preauthorization;

interface PreauthorizationResponseInterface
{
    /**
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * @param string $id
     * @return PreauthorizationResponseInterface
     */
    public function setId(string $id): PreauthorizationResponseInterface;

    /**
     * @return string|null
     */
    public function getSellerId(): ?string;

    /**
     * @param string $sellerId
     * @return PreauthorizationResponseInterface
     */
    public function setSellerId(string $sellerId): PreauthorizationResponseInterface;

    /**
     * @return string|null
     */
    public function getBuyerId(): ?string;

    /**
     * @param string $buyerId
     * @return PreauthorizationResponseInterface
     */
    public function setBuyerId(string $buyerId): PreauthorizationResponseInterface;

    /**
     * @return string|null
     */
    public function getCurrency(): ?string;

    /**
     * @param string $currency
     * @return PreauthorizationResponseInterface
     */
    public function setCurrency(string $currency): PreauthorizationResponseInterface;

    /**
     * @return int|null
     */
    public function getPreauthorizedAmount(): ?int;

    /**
     * @param int $preauthorizedAmount
     * @return PreauthorizationResponseInterface
     */
    public function setPreauthorizedAmount(int $preauthorizedAmount): PreauthorizationResponseInterface;

    /**
     * @return int|null
     */
    public function getCapturedAmount(): ?int;

    /**
     * @param int $capturedAmount
     * @return PreauthorizationResponseInterface
     */
    public function setCapturedAmount(int $capturedAmount): PreauthorizationResponseInterface;

    /**
     * @return int|null
     */
    public function getForeignExchangeFee(): ?int;

    /**
     * @param int $foreignExchangeFee
     * @return PreauthorizationResponseInterface
     */
    public function setForeignExchangeFee(int $foreignExchangeFee): PreauthorizationResponseInterface;

    /**
     * @return string|null
     */
    public function getStatus(): ?string;

    /**
     * @param string $status
     * @return PreauthorizationResponseInterface
     */
    public function setStatus(string $status): PreauthorizationResponseInterface;

    /**
     * @return string|null
     */
    public function getPoNumber(): ?string;

    /**
     * @param string $poNumber
     * @return PreauthorizationResponseInterface
     */
    public function setPoNumber(string $poNumber): PreauthorizationResponseInterface;

    /**
     * @return string|null
     */
    public function getExpires(): ?string;

    /**
     * @param string $expires
     * @return PreauthorizationResponseInterface
     */
    public function setExpires(string $expires): PreauthorizationResponseInterface;

    /**
     * @return string|null
     */
    public function getCreated(): ?string;

    /**
     * @param string $created
     * @return PreauthorizationResponseInterface
     */
    public function setCreated(string $created): PreauthorizationResponseInterface;

    /**
     * @return string|null
     */
    public function getModified(): ?string;

    /**
     * @param string $modified
     * @return PreauthorizationResponseInterface
     */
    public function setModified(string $modified): PreauthorizationResponseInterface;

    /**
     * @return array
     */
    public function getRequestData(): array;
}
