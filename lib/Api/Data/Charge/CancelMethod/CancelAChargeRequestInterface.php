<?php
declare(strict_types=1);

namespace Msts\CaaS\Api\Data\Charge\CancelMethod;

interface CancelAChargeRequestInterface
{
    /**
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * @param string $id
     * @return CancelAChargeRequestInterface
     */
    public function setId(string $id): CancelAChargeRequestInterface;

    /**
     * @return string|null
     */
    public function getReason(): ?string;

    /**
     * @param string $reason
     * @return CancelAChargeRequestInterface
     */
    public function setReason(string $reason): CancelAChargeRequestInterface;

    /**
     * @return array
     */
    public function getRequestData(): array;
}
