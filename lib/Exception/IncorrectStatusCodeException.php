<?php
declare(strict_types=1);

namespace Msts\CaaS\Exception;

class IncorrectStatusCodeException extends ApiClientException
{
}
