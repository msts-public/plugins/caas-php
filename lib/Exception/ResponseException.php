<?php
declare(strict_types=1);

namespace Msts\CaaS\Exception;

class ResponseException extends ApiClientException
{
}
