<?php
declare(strict_types=1);

namespace Msts\CaaS;

use Msts\CaaS\Exception\ApiClientException;
use Msts\CaaS\Http\Transfer;

interface ApiClientInterface
{
    /**
     * @param Transfer $transfer
     * @return array
     * @throws ApiClientException
     */
    public function execute(Transfer $transfer): array;
}
