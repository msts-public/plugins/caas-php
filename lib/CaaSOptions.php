<?php
declare(strict_types=1);

namespace Msts\CaaS;

use Monolog\Logger;
use Msts\CaaS\Http\TransferBuilder;
use Msts\CaaS\Model\ClientConfigProvider;
use Msts\CaaS\Model\Http\MstsRequest;
use Msts\CaaS\Model\MaskValue;
use Psr\Log\LoggerInterface;

class CaaSOptions
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var MaskValue
     */
    private $maskValue;

    /**
     * @var MstsRequest
     */
    private $mstsRequest;

    /**
     * @var ClientConfigProvider
     */
    private $configProvider;

    public function __construct()
    {
        $this->logger = new Logger('api_client');
        $this->maskValue = new MaskValue();

        $this->configProvider = new ClientConfigProvider($this->maskValue);
        $this->configProvider->setBaseUri('https://app.msts.credit/');
        $this->mstsRequest = new MstsRequest(
            new TransferBuilder(),
            $this->configProvider,
            $this->maskValue
        );
    }

    public function setHost(string $host): CaaSOptions
    {
        $this->configProvider->setBaseUri($host);

        return $this;
    }

    public function setLogger(LoggerInterface $logger): CaaSOptions
    {
        $this->logger = $logger;

        return $this;
    }

    public function setMaskValue(MaskValue $maskValue): CaaSOptions
    {
        $this->maskValue = $maskValue;

        return $this;
    }

    public function setRequestClass(MstsRequest $mstsRequest): CaaSOptions
    {
        $this->mstsRequest = $mstsRequest;

        return $this;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    /**
     * @return MaskValue
     */
    public function getMaskValue(): MaskValue
    {
        return $this->maskValue;
    }

    /**
     * @return MstsRequest
     */
    public function getMstsRequest(): MstsRequest
    {
        return $this->mstsRequest;
    }
}
