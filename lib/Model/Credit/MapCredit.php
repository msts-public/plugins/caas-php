<?php

declare(strict_types=1);

namespace Msts\CaaS\Model\Credit;

use Msts\CaaS\Api\Data\Credit\CreditResponseInterface;
use Msts\CaaS\Model\Data\Credit\CreditDetail;
use Msts\CaaS\Model\Data\Credit\CreditResponse;

class MapCredit
{
    public function execute(array $entries): array
    {
        $resultEntries = [];

        foreach ($entries as $entry) {
            /** @var CreditResponseInterface $credit */
            $credit = new CreditResponse($entry);
            $credit->setDetails($this->prepareDetails($entry));
            $resultEntries[] = $credit;
        }

        return $resultEntries;
    }

    private function prepareDetails(array $entry): array
    {
        $result = [];

        if (!isset($entry['details'])) {
            return $result;
        }
        foreach ($entry['details'] as $detailSource) {
            $detailSource['quantity'] = (float)$detailSource['quantity'];
            $result[] = new CreditDetail($detailSource);
        }

        return $result;
    }
}
