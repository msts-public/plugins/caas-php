<?php
declare(strict_types=1);

namespace Msts\CaaS\Model\Credit;

use Msts\CaaS\Api\Data\Credit\CreditResponseInterface;
use Msts\CaaS\ApiClientInterface;
use Msts\CaaS\Exception\ApiClientException;
use Msts\CaaS\Exception\ResponseException;
use Msts\CaaS\Model\Http\MstsRequest;

class CreditApiCall
{
    private const METHOD_NAME = 'credits';

    /**
     * @var ApiClientInterface
     */
    private $apiClient;

    /**
     * @var MapCredit
     */
    private $mapCredit;

    /**
     * @var MstsRequest
     */
    private $mstsRequest;

    public function __construct(
        ApiClientInterface $apiClient,
        MapCredit $mapCredit,
        MstsRequest $mstsRequest
    ) {
        $this->apiClient = $apiClient;
        $this->mapCredit = $mapCredit;
        $this->mstsRequest = $mstsRequest;
    }

    /**
     * @param array $requestData
     * @return CreditResponseInterface
     * @throws ApiClientException
     */
    public function create(array $requestData): CreditResponseInterface
    {
        $transfer = $this->mstsRequest->create(
            self::METHOD_NAME,
            $requestData,
            null,
            'POST',
            201
        );
        $result = $this->apiClient->execute($transfer);
        $creditList = $this->mapCredit->execute([$result]);

        if (!isset($creditList[0])) {
            throw new ResponseException('Credit Note was not returned');
        }

        return $creditList[0];
    }

    /**
     * @param array $requestData
     * @return CreditResponseInterface[]
     * @throws ApiClientException
     */
    public function list(array $requestData): array
    {
        $transfer = $this->mstsRequest->create(
            self::METHOD_NAME,
            $requestData
        );
        $result = $this->apiClient->execute($transfer);

        return $this->mapCredit->execute($result);
    }

    /**
     * @param string $id
     * @return CreditResponseInterface
     * @throws ApiClientException
     */
    public function retrieve(string $id): CreditResponseInterface
    {
        $transfer = $this->mstsRequest->create(
            self::METHOD_NAME,
            [],
            $id
        );
        $result = $this->apiClient->execute($transfer);

        $creditList = $this->mapCredit->execute([$result]);

        if (!isset($creditList[0])) {
            throw new ResponseException('Credit was not returned');
        }

        return $creditList[0];
    }
}
