<?php

declare(strict_types=1);

namespace Msts\CaaS\Model\Data\Credit;

use Msts\CaaS\Api\Data\Credit\CreditResponseInterface;
use Msts\CaaS\Api\Data\Credit\CreditDetailInterface;
use Msts\CaaS\Model\RequestDataObject;

/**
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 */
class CreditResponse extends RequestDataObject implements CreditResponseInterface
{
    /**
     * Identifier of a Credit
     */
    private const ID = 'id';
    private const CHARGE_ID = 'charge_id';
    private const SELLER_ID = 'seller_id';
    private const BUYER_ID = 'buyer_id';
    private const STATUS = 'status';
    private const CURRENCY = 'currency';
    private const TOTAL_AMOUNT = 'total_amount';
    private const TAX_AMOUNT = 'tax_amount';
    private const SHIPPING_AMOUNT = 'shipping_amount';
    private const SHIPPING_TAX_AMOUNT = 'shipping_tax_amount';
    private const SHIPPING_TAX_DETAILS = 'shipping_tax_details';
    private const SHIPPING_DISCOUNT_AMOUNT = 'shipping_discount_amount';
    private const DISCOUNT_AMOUNT = 'discount_amount';
    private const CREDIT_AMOUNT = 'credit_amount';
    private const CREDIT_AMOUNT_CURRENCY = 'credit_amount_currency';
    private const CREDIT_REASON = 'credit_reason';
    private const CREDIT_COMMENT = 'credit_comment';
    private const DETAILS = 'details';

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->getData(self::ID);
    }

    /**
     * @param string $id
     * @return CreditResponseInterface
     */
    public function setId(string $id): CreditResponseInterface
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * @return string|null
     */
    public function getChargeId(): ?string
    {
        return $this->getData(self::CHARGE_ID);
    }

    /**
     * @param string $setChargeId
     * @return CreditResponseInterface
     */
    public function setChargeId(string $setChargeId): CreditResponseInterface
    {
        return $this->setData(self::CHARGE_ID, $setChargeId);
    }

    /**
     * @return string|null
     */
    public function getSellerId(): ?string
    {
        return $this->getData(self::SELLER_ID);
    }

    /**
     * @param string $sellerId
     * @return CreditResponseInterface
     */
    public function setSellerId(string $sellerId): CreditResponseInterface
    {
        return $this->setData(self::SELLER_ID, $sellerId);
    }

    /**
     * @return string|null
     */
    public function getBuyerId(): ?string
    {
        return $this->getData(self::BUYER_ID);
    }

    /**
     * @param string $buyerId
     * @return CreditResponseInterface
     */
    public function setBuyerId(string $buyerId): CreditResponseInterface
    {
        return $this->setData(self::BUYER_ID, $buyerId);
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->getData(self::STATUS);
    }

    /**
     * @param string $status
     * @return CreditResponseInterface
     */
    public function setStatus(string $status): CreditResponseInterface
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->getData(self::CURRENCY);
    }

    /**
     * @param string $currency
     * @return CreditResponseInterface
     */
    public function setCurrency(string $currency): CreditResponseInterface
    {
        return $this->setData(self::CURRENCY, $currency);
    }

    /**
     * @return int|null
     */
    public function getTotalAmount(): ?int
    {
        return $this->getData(self::TOTAL_AMOUNT);
    }

    /**
     * @param int $totalAmount
     * @return CreditResponseInterface
     */
    public function setTotalAmount(int $totalAmount): CreditResponseInterface
    {
        return $this->setData(self::TOTAL_AMOUNT, $totalAmount);
    }

    /**
     * @return int|null
     */
    public function getTaxAmount(): ?int
    {
        return $this->getData(self::TAX_AMOUNT);
    }

    /**
     * @param int $taxAmount
     * @return CreditResponseInterface
     */
    public function setTaxAmount(int $taxAmount): CreditResponseInterface
    {
        return $this->setData(self::TAX_AMOUNT, $taxAmount);
    }

    /**
     * @return int|null
     */
    public function getShippingAmount(): ?int
    {
        return $this->getData(self::SHIPPING_AMOUNT);
    }

    /**
     * @param int $shippingAmount
     * @return CreditResponseInterface
     */
    public function setShippingAmount(int $shippingAmount): CreditResponseInterface
    {
        return $this->setData(self::SHIPPING_AMOUNT, $shippingAmount);
    }

    /**
     * @return int|null
     */
    public function getShippingTaxAmount(): ?int
    {
        return $this->getData(self::SHIPPING_TAX_AMOUNT);
    }

    /**
     * @param int $shippingTaxAmount
     * @return CreditResponseInterface
     */
    public function setShippingTaxAmount(int $shippingTaxAmount): CreditResponseInterface
    {
        return $this->setData(self::SHIPPING_TAX_AMOUNT, $shippingTaxAmount);
    }

    /**
     * @return array|null
     */
    public function getShippingTaxDetails(): ?array
    {
        return $this->getData(self::SHIPPING_TAX_DETAILS);
    }

    /**
     * @param array|null $shippingTaxDetails
     * @return CreditResponseInterface
     */
    public function setShippingTaxDetails($shippingTaxDetails): CreditResponseInterface
    {
        return $this->setData(self::SHIPPING_TAX_DETAILS, $shippingTaxDetails);
    }

    /**
     * @return int|null
     */
    public function getShippingDiscountAmount(): ?int
    {
        return $this->getData(self::SHIPPING_DISCOUNT_AMOUNT);
    }

    /**
     * @param int $shippingDiscountAmount
     * @return CreditResponseInterface
     */
    public function setShippingDiscountAmount(int $shippingDiscountAmount): CreditResponseInterface
    {
        return $this->setData(self::SHIPPING_DISCOUNT_AMOUNT, $shippingDiscountAmount);
    }

    /**
     * @return int|null
     */
    public function getDiscountAmount(): ?int
    {
        return $this->getData(self::DISCOUNT_AMOUNT);
    }

    /**
     * @param int $discountAmount
     * @return CreditResponseInterface
     */
    public function setDiscountAmount(int $discountAmount): CreditResponseInterface
    {
        return $this->setData(self::DISCOUNT_AMOUNT, $discountAmount);
    }

    /**
     * @return string|null
     */
    public function getCreditAmountCurrency(): ?string
    {
        return $this->getData(self::CREDIT_AMOUNT_CURRENCY);
    }

    /**
     * @param string $creditAmountCurrency
     * @return CreditResponseInterface
     */
    public function setCreditAmountCurrency(string $creditAmountCurrency): CreditResponseInterface
    {
        return $this->setData(self::CREDIT_AMOUNT_CURRENCY, $creditAmountCurrency);
    }


    /**
     * @return int|null
     */
    public function getCreditAmount(): ?int
    {
        return $this->getData(self::CREDIT_AMOUNT);
    }

    /**
     * @param int $creditAmount
     * @return CreditResponseInterface
     */
    public function setCreditAmount(int $creditAmount): CreditResponseInterface
    {
        return $this->setData(self::CREDIT_AMOUNT, $creditAmount);
    }

    /**
     * @return string|null
     */
    public function getCreditComment(): ?string
    {
        return $this->getData(self::CREDIT_COMMENT);
    }

    /**
     * @param string $creditComment
     * @return CreditResponseInterface
     */
    public function setCreditComment(string $creditComment): CreditResponseInterface
    {
        return $this->setData(self::CREDIT_COMMENT, $creditComment);
    }

    /**
     * @return string|null
     */
    public function getCreditReason(): ?string
    {
        return $this->getData(self::CREDIT_REASON);
    }

    /**
     * @param string $creditReason
     * @return CreditResponseInterface
     */
    public function setCreditReason(string $creditReason): CreditResponseInterface
    {
        return $this->setData(self::CREDIT_REASON, $creditReason);
    }

    /**
     * @return CreditDetailInterface[]
     */
    public function getDetails(): ?array
    {
        return $this->getData(self::DETAILS);
    }

    /**
     * @param array $items
     * @return CreditResponseInterface
     */
    public function setDetails(array $items): CreditResponseInterface
    {
        return $this->setData(self::DETAILS, $items);
    }

    public function getRequestData(): array
    {
        $requestData = parent::getRequestData();
        if ($this->getData(self::DETAILS)) {
            $details = [];
            /** @var CreditDetailInterface $detail */
            foreach ($this->getData(self::DETAILS) as $detail) {
                $details[] = $detail->getRequestData();
            }
            $requestData[self::DETAILS] = $details;
        }


        return $requestData;
    }
}
