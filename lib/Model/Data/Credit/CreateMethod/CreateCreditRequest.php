<?php

declare(strict_types=1);

namespace Msts\CaaS\Model\Data\Credit\CreateMethod;

use Msts\CaaS\Api\Data\Credit\CreateMethod\CreateCreditRequestInterface;
use Msts\CaaS\Model\Data\Credit\CreditResponse;

class CreateCreditRequest extends CreditResponse implements CreateCreditRequestInterface
{
}
