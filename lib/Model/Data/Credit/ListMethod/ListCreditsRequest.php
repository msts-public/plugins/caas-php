<?php

declare(strict_types=1);

namespace Msts\CaaS\Model\Data\Credit\ListMethod;

use Msts\CaaS\Api\Data\Credit\ListMethod\ListCreditsRequestInterface;
use Msts\CaaS\Model\RequestDataObject;

class ListCreditsRequest extends RequestDataObject implements ListCreditsRequestInterface
{
    private const CHARGE_ID = 'charge_id';
    private const PAGE_SIZE = 'page_size';
    private const PAGE_NUMBER = 'page_number';
    private const FROM_DATE = 'from_date';
    private const TO_DATE = 'to_date';

    /**
     * @return int|null
     */
    public function getChargeId(): ?int
    {
        return $this->getData(self::CHARGE_ID);
    }

    /**
     * @param int $chargeId
     * @return ListCreditsRequestInterface
     */
    public function setChargeId(int $chargeId): ListCreditsRequestInterface
    {
        $this->setData(self::CHARGE_ID, $chargeId);

        return $this;
    }


    /**
     * @return int|null
     */
    public function getPageSize(): ?int
    {
        return $this->getData(self::PAGE_SIZE);
    }

    /**
     * @param int $pageSize
     * @return ListCreditsRequestInterface
     */
    public function setPageSize(int $pageSize): ListCreditsRequestInterface
    {
        $this->setData(self::PAGE_SIZE, $pageSize);

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPageNumber(): ?int
    {
        return $this->getData(self::PAGE_NUMBER);
    }

    /**
     * @param int $pageNumber
     * @return ListCreditsRequestInterface
     */
    public function setPageNumber(int $pageNumber): ListCreditsRequestInterface
    {
        $this->setData(self::PAGE_NUMBER, $pageNumber);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFromData(): ?string
    {
        return $this->getData(self::FROM_DATE);
    }

    /**
     * @param string $fromDate
     * @return ListCreditsRequestInterface
     */
    public function setFromData(string $fromDate): ListCreditsRequestInterface
    {
        $this->setData(self::FROM_DATE, $fromDate);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getToData(): ?string
    {
        return $this->getData(self::TO_DATE);
    }

    /**
     * @param string $toData
     * @return ListCreditsRequestInterface
     */
    public function setToData(string $toData): ListCreditsRequestInterface
    {
        $this->setData(self::TO_DATE, $toData);

        return $this;
    }
}
