<?php
declare(strict_types=1);

namespace Msts\CaaS\Model\Data\Charge\CreateMethod;

use Msts\CaaS\Api\Data\Charge\CreateMethod\CreateAChargeRequestInterface;
use Msts\CaaS\Model\Data\Charge\ChargeResponse;

class CreateAChargeRequest extends ChargeResponse implements CreateAChargeRequestInterface
{
    private const PRE_AUTHORIZATION_ID = 'preauthorization_id';

    /**
     * @param string $preAuthorizationId
     * @return CreateAChargeRequestInterface
     */
    public function setPreauthorizationId(string $preAuthorizationId): CreateAChargeRequestInterface
    {
        return $this->setData(self::PRE_AUTHORIZATION_ID, $preAuthorizationId);
    }
}
