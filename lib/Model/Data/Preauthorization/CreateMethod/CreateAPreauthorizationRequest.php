<?php
declare(strict_types=1);

namespace Msts\CaaS\Model\Data\Preauthorization\CreateMethod;

use Msts\CaaS\Api\Data\Preauthorization\CreateMethod\CreateAPreauthorizationRequestInterface;
use Msts\CaaS\Model\RequestDataObject;

class CreateAPreauthorizationRequest extends RequestDataObject implements CreateAPreauthorizationRequestInterface
{
    private const SELLER_ID = 'seller_id';

    private const BUYER_ID = 'buyer_id';

    private const CURRENCY = 'currency';

    private const PREAUTHORIZED_AMOUNT = 'preauthorized_amount';

    private const PO_NUMBER = 'po_number';

    /**
     * @return string|null
     */
    public function getSellerId(): ?string
    {
        return $this->getData(self::SELLER_ID);
    }

    /**
     * @param string $sellerId
     * @return CreateAPreauthorizationRequestInterface
     */
    public function setSellerId(string $sellerId): CreateAPreauthorizationRequestInterface
    {
        return $this->setData(self::SELLER_ID, $sellerId);
    }

    /**
     * @return string|null
     */
    public function getBuyerId(): ?string
    {
        return $this->getData(self::BUYER_ID);
    }

    /**
     * @param string $buyerId
     * @return CreateAPreauthorizationRequestInterface
     */
    public function setBuyerId(string $buyerId): CreateAPreauthorizationRequestInterface
    {
        return $this->setData(self::BUYER_ID, $buyerId);
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->getData(self::CURRENCY);
    }

    /**
     * @param string $currency
     * @return CreateAPreauthorizationRequestInterface
     */
    public function setCurrency(string $currency): CreateAPreauthorizationRequestInterface
    {
        return $this->setData(self::CURRENCY, $currency);
    }

    /**
     * @return int|null
     */
    public function getPreauthorizedAmount(): ?int
    {
        return $this->getData(self::PREAUTHORIZED_AMOUNT);
    }

    /**
     * @param int $preauthorizedAmount
     * @return CreateAPreauthorizationRequestInterface
     */
    public function setPreauthorizedAmount(int $preauthorizedAmount): CreateAPreauthorizationRequestInterface
    {
        return $this->setData(self::PREAUTHORIZED_AMOUNT, $preauthorizedAmount);
    }

    /**
     * @return string|null
     */
    public function getPoNumber(): ?string
    {
        return $this->getData(self::PO_NUMBER);
    }

    /**
     * @param string $poNumber
     * @return CreateAPreauthorizationRequestInterface
     */
    public function setPoNumber(string $poNumber): CreateAPreauthorizationRequestInterface
    {
        return $this->setData(self::PO_NUMBER, $poNumber);
    }
}
