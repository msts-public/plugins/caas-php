<?php
declare(strict_types=1);

namespace Msts\CaaS\Model\Data\Preauthorization\UpdateMethod;

use Msts\CaaS\Api\Data\Preauthorization\UpdateMethod\UpdateAPreauthorizationRequestInterface;
use Msts\CaaS\Model\RequestDataObject;

class UpdateAPreauthorizationRequest extends RequestDataObject implements UpdateAPreauthorizationRequestInterface
{
    private const ID = 'id';

    private const PREAUTHORIZED_AMOUNT = 'preauthorized_amount';

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->getData(self::ID);
    }

    /**
     * @param string $id
     * @return UpdateAPreauthorizationRequestInterface
     */
    public function setId(string $id): UpdateAPreauthorizationRequestInterface
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * @return string|null
     */
    public function getPreauthorizedAmount(): ?string
    {
        return $this->getData(self::PREAUTHORIZED_AMOUNT);
    }

    /**
     * @param int $preauthorizedAmount
     * @return UpdateAPreauthorizationRequestInterface
     */
    public function setPreauthorizedAmount(int $preauthorizedAmount): UpdateAPreauthorizationRequestInterface
    {
        return $this->setData(self::PREAUTHORIZED_AMOUNT, $preauthorizedAmount);
    }
}
