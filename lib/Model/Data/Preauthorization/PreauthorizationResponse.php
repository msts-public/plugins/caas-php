<?php
declare(strict_types=1);

namespace Msts\CaaS\Model\Data\Preauthorization;

use Msts\CaaS\Api\Data\Preauthorization\PreauthorizationResponseInterface;
use Msts\CaaS\Model\RequestDataObject;

class PreauthorizationResponse extends RequestDataObject implements PreauthorizationResponseInterface
{
    private const ID = 'id';

    private const SELLER_ID = 'seller_id';

    private const BUYER_ID = 'buyer_id';

    private const CURRENCY = 'currency';

    private const PREAUTHORIZED_AMOUNT = 'preauthorized_amount';

    private const CAPTURED_AMOUNT = 'captured_amount';

    private const FOREIGN_EXCHANGE_FEE = 'foreign_exchange_fee';

    private const STATUS = 'status';

    private const PO_NUMBER = 'po_number';

    private const EXPIRES = 'expires';

    private const CREATED = 'created';

    private const MODIFIED = 'modified';

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->getData(self::ID);
    }

    /**
     * @param string $id
     * @return PreauthorizationResponseInterface
     */
    public function setId(string $id): PreauthorizationResponseInterface
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * @return string|null
     */
    public function getSellerId(): ?string
    {
        return $this->getData(self::SELLER_ID);
    }

    /**
     * @param string $sellerId
     * @return PreauthorizationResponseInterface
     */
    public function setSellerId(string $sellerId): PreauthorizationResponseInterface
    {
        return $this->setData(self::SELLER_ID, $sellerId);
    }

    /**
     * @return string|null
     */
    public function getBuyerId(): ?string
    {
        return $this->getData(self::BUYER_ID);
    }

    /**
     * @param string $buyerId
     * @return PreauthorizationResponseInterface
     */
    public function setBuyerId(string $buyerId): PreauthorizationResponseInterface
    {
        return $this->setData(self::BUYER_ID, $buyerId);
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->getData(self::CURRENCY);
    }

    /**
     * @param string $currency
     * @return PreauthorizationResponseInterface
     */
    public function setCurrency(string $currency): PreauthorizationResponseInterface
    {
        return $this->setData(self::CURRENCY, $currency);
    }

    /**
     * @return int|null
     */
    public function getPreauthorizedAmount(): ?int
    {
        return $this->getData(self::PREAUTHORIZED_AMOUNT);
    }

    /**
     * @param int $preauthorizedAmount
     * @return PreauthorizationResponseInterface
     */
    public function setPreauthorizedAmount(int $preauthorizedAmount): PreauthorizationResponseInterface
    {
        return $this->setData(self::PREAUTHORIZED_AMOUNT, $preauthorizedAmount);
    }

    /**
     * @return int|null
     */
    public function getCapturedAmount(): ?int
    {
        return $this->getData(self::CAPTURED_AMOUNT);
    }

    /**
     * @param int $capturedAmount
     * @return PreauthorizationResponseInterface
     */
    public function setCapturedAmount(int $capturedAmount): PreauthorizationResponseInterface
    {
        return $this->setData(self::CAPTURED_AMOUNT, $capturedAmount);
    }

    /**
     * @return int|null
     */
    public function getForeignExchangeFee(): ?int
    {
        return $this->getData(self::FOREIGN_EXCHANGE_FEE);
    }

    /**
     * @param int $foreignExchangeFee
     * @return PreauthorizationResponseInterface
     */
    public function setForeignExchangeFee(int $foreignExchangeFee): PreauthorizationResponseInterface
    {
        return $this->setData(self::FOREIGN_EXCHANGE_FEE, $foreignExchangeFee);
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->getData(self::STATUS);
    }

    /**
     * @param string $status
     * @return PreauthorizationResponseInterface
     */
    public function setStatus(string $status): PreauthorizationResponseInterface
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * @return string|null
     */
    public function getPoNumber(): ?string
    {
        return $this->getData(self::PO_NUMBER);
    }

    /**
     * @param string $poNumber
     * @return PreauthorizationResponseInterface
     */
    public function setPoNumber(string $poNumber): PreauthorizationResponseInterface
    {
        return $this->setData(self::PO_NUMBER, $poNumber);
    }

    /**
     * @return string|null
     */
    public function getExpires(): ?string
    {
        return $this->getData(self::EXPIRES);
    }

    /**
     * @param string $expires
     * @return PreauthorizationResponseInterface
     */
    public function setExpires(string $expires): PreauthorizationResponseInterface
    {
        return $this->setData(self::EXPIRES, $expires);
    }

    /**
     * @return string|null
     */
    public function getCreated(): ?string
    {
        return $this->getData(self::CREATED);
    }

    /**
     * @param string $created
     * @return PreauthorizationResponseInterface
     */
    public function setCreated(string $created): PreauthorizationResponseInterface
    {
        return $this->setData(self::CREATED, $created);
    }

    /**
     * @return string|null
     */
    public function getModified(): ?string
    {
        return $this->getData(self::MODIFIED);
    }

    /**
     * @param string $modified
     * @return PreauthorizationResponseInterface
     */
    public function setModified(string $modified): PreauthorizationResponseInterface
    {
        return $this->setData(self::MODIFIED, $modified);
    }
}
