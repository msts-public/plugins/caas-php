<?php
declare(strict_types=1);

namespace Msts\CaaS\Model\Preauthorization;

use Msts\CaaS\Api\Data\Preauthorization\PreauthorizationResponseInterface;
use Msts\CaaS\ApiClientInterface;
use Msts\CaaS\Exception\ApiClientException;
use Msts\CaaS\Model\Data\Preauthorization\PreauthorizationResponse;
use Msts\CaaS\Model\Http\MstsRequest;

class PreauthorizationApiCall
{
    private const METHOD_NAME = 'preauthorizations';

    /**
     * @var ApiClientInterface
     */
    private $apiClient;

    /**
     * @var MstsRequest
     */
    private $mstsRequest;

    public function __construct(
        ApiClientInterface $apiClient,
        MstsRequest $mstsRequest
    ) {
        $this->apiClient = $apiClient;
        $this->mstsRequest = $mstsRequest;
    }

    /**
     * @param array $requestData
     * @return PreauthorizationResponseInterface
     * @throws ApiClientException
     */
    public function create(
        array $requestData
    ): PreauthorizationResponseInterface {
        $transfer = $this->mstsRequest->create(
            self::METHOD_NAME,
            $requestData,
            null,
            'POST',
            201
        );

        $result = $this->apiClient->execute($transfer);

        return new PreauthorizationResponse($result);
    }

    /**
     * @param string $id
     * @return PreauthorizationResponseInterface
     * @throws ApiClientException
     */
    public function cancel(string $id): PreauthorizationResponseInterface
    {
        $transfer = $this->mstsRequest->create(
            self::METHOD_NAME,
            [],
            $id,
            'DELETE'
        );
        $result = $this->apiClient->execute($transfer);

        return new PreauthorizationResponse($result);
    }

    /**
     * @param array $requestData
     * @return PreauthorizationResponseInterface
     * @throws ApiClientException
     */
    public function update(
        array $requestData
    ): PreauthorizationResponseInterface {
        if (!isset($requestData['id'])) {
            throw new ApiClientException('ID is required');
        }
        $transfer = $this->mstsRequest->create(
            self::METHOD_NAME,
            $requestData,
            $requestData['id'],
            'POST'
        );
        $result = $this->apiClient->execute($transfer);

        return new PreauthorizationResponse($result);
    }
}
