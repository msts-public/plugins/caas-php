<?php
declare(strict_types=1);

namespace Msts\CaaS\Model\Charge;

use Msts\CaaS\Api\Data\Charge\ChargeResponseInterface;
use Msts\CaaS\Model\Data\Charge\ChargeDetail;
use Msts\CaaS\Model\Data\Charge\ChargeResponse;

class MapCharge
{
    public function execute(array $entries): array
    {
        $resultEntries = [];
        foreach ($entries as $entry) {
            /** @var ChargeResponseInterface $charge */
            $charge = new ChargeResponse($entry);
            $charge->setDetails($this->prepareDetails($entry));
            $resultEntries[] = $charge;
        }

        return $resultEntries;
    }

    private function prepareDetails(array $entry): array
    {
        $result = [];

        if (!isset($entry['details'])) {
            return $result;
        }
        foreach ($entry['details'] as $detailSource) {
            $detailSource['quantity'] = (float)$detailSource['quantity'];
            $result[] = new ChargeDetail($detailSource);
        }

        return $result;
    }
}
