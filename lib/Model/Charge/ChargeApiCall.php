<?php
declare(strict_types=1);

namespace Msts\CaaS\Model\Charge;

use Msts\CaaS\Api\Data\Charge\ChargeResponseInterface;
use Msts\CaaS\ApiClientInterface;
use Msts\CaaS\Exception\ApiClientException;
use Msts\CaaS\Exception\ResponseException;
use Msts\CaaS\Model\Http\MstsRequest;

class ChargeApiCall
{
    private const METHOD_NAME = 'charges';

    /**
     * @var ApiClientInterface
     */
    private $apiClient;

    /**
     * @var MapCharge
     */
    private $mapCharge;

    /**
     * @var MstsRequest
     */
    private $mstsRequest;

    public function __construct(
        ApiClientInterface $apiClient,
        MapCharge $mapCharge,
        MstsRequest $mstsRequest
    ) {
        $this->apiClient = $apiClient;
        $this->mapCharge = $mapCharge;
        $this->mstsRequest = $mstsRequest;
    }

    /**
     * @param array $requestData
     * @return ChargeResponseInterface
     * @throws ApiClientException
     */
    public function create(array $requestData): ChargeResponseInterface
    {
        $transfer = $this->mstsRequest->create(
            self::METHOD_NAME,
            $requestData,
            null,
            'POST',
            201
        );
        $result = $this->apiClient->execute($transfer);

        $chargeList = $this->mapCharge->execute([$result]);

        if (!isset($chargeList[0])) {
            throw new ResponseException('Charge was not returned');
        }

        return $chargeList[0];
    }

    /**
     * @param array $requestData
     * @return ChargeResponseInterface[]
     * @throws ApiClientException
     */
    public function list(array $requestData): array
    {
        $transfer = $this->mstsRequest->create(
            self::METHOD_NAME,
            $requestData
        );
        $result = $this->apiClient->execute($transfer);

        return $this->mapCharge->execute($result);
    }

    /**
     * @param array $requestData
     * @return ChargeResponseInterface
     * @throws ApiClientException
     */
    public function cancel(array $requestData): ChargeResponseInterface
    {
        if (!isset($requestData['id'])) {
            throw new ApiClientException('ID is required');
        }
        $id = $requestData['id'];
        unset($requestData['id']);
        $transfer = $this->mstsRequest->create(
            self::METHOD_NAME,
            $requestData,
            $id,
            'DELETE'
        );
        $result = $this->apiClient->execute($transfer);

        $chargeList = $this->mapCharge->execute([$result]);

        if (!isset($chargeList[0])) {
            throw new ResponseException('Charge was not returned');
        }

        return $chargeList[0];
    }

    /**
     * @param string $id
     * @return ChargeResponseInterface
     * @throws ApiClientException
     */
    public function retrieve(string $id): ChargeResponseInterface
    {
        $transfer = $this->mstsRequest->create(
            self::METHOD_NAME,
            [],
            $id
        );
        $result = $this->apiClient->execute($transfer);

        $chargeList = $this->mapCharge->execute([$result]);

        if (!isset($chargeList[0])) {
            throw new ResponseException('Charge was not returned');
        }

        return $chargeList[0];
    }

    /**
     * @param array $requestData
     * @return ChargeResponseInterface
     * @throws ApiClientException
     * @throws ResponseException
     */
    public function return(array $requestData): ChargeResponseInterface
    {
        if (!isset($requestData['id'])) {
            throw new ApiClientException('ID is required');
        }
        $transfer = $this->mstsRequest->create(
            self::METHOD_NAME,
            $requestData,
            $requestData['id'],
            'POST',
            201
        );
        $result = $this->apiClient->execute($transfer);

        $chargeList = $this->mapCharge->execute([$result]);

        if (!isset($chargeList[0])) {
            throw new ResponseException('Charge was not returned');
        }

        return $chargeList[0];
    }
}
