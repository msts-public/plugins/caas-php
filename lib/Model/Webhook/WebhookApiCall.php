<?php
declare(strict_types=1);

namespace Msts\CaaS\Model\Webhook;

use Msts\CaaS\Api\Data\Webhook\WebhookInterface;
use Msts\CaaS\ApiClientInterface;
use Msts\CaaS\Exception\ApiClientException;
use Msts\CaaS\Model\Data\Webhook\Webhook;
use Msts\CaaS\Model\Http\MstsRequest;

class WebhookApiCall
{
    public const METHOD_NAME = 'webhooks';

    /**
     * @var ApiClientInterface
     */
    private $apiClient;

    /**
     * @var MstsRequest
     */
    private $mstsRequest;

    public function __construct(
        ApiClientInterface $apiClient,
        MstsRequest $mstsRequest
    ) {
        $this->apiClient = $apiClient;
        $this->mstsRequest = $mstsRequest;
    }

    /**
     * @param array $requestData
     * @return WebhookInterface
     * @throws ApiClientException
     */
    public function create(
        array $requestData
    ): WebhookInterface {
        $transfer = $this->mstsRequest->create(
            self::METHOD_NAME,
            $requestData,
            null,
            'POST',
            201
        );

        $result = $this->apiClient->execute($transfer);

        return new Webhook($result);
    }

    /**
     * @return WebhookInterface[]
     * @throws ApiClientException
     */
    public function list(): array
    {
        $transfer = $this->mstsRequest->create(
            self::METHOD_NAME
        );

        $result = $this->apiClient->execute($transfer);

        $webhooks = [];
        foreach ($result as $item) {
            $webhooks[] = new Webhook($item);
        }

        return $webhooks;
    }

    /**
     * @param string $id
     * @return WebhookInterface
     * @throws ApiClientException
     */
    public function retrieve(string $id): WebhookInterface
    {
        $transfer = $this->mstsRequest->create(
            self::METHOD_NAME,
            [],
            $id
        );

        $result = $this->apiClient->execute($transfer);

        return new Webhook($result);
    }

    /**
     * @param array $requestData
     * @return WebhookInterface
     * @throws ApiClientException
     */
    public function update(array $requestData): WebhookInterface
    {
        if (!isset($requestData['id'])) {
            throw new ApiClientException('ID is required');
        }
        $transfer = $this->mstsRequest->create(
            self::METHOD_NAME,
            $requestData,
            $requestData['id'],
            'POST'
        );

        $result = $this->apiClient->execute($transfer);

        return new Webhook($result);
    }

    /**
     * @param string $id
     * @param string|null $apiKey
     * @return WebhookInterface
     * @throws ApiClientException
     */
    public function delete(string $id, ?string $apiKey = null): WebhookInterface
    {
        $transfer = $this->mstsRequest->create(
            self::METHOD_NAME,
            [],
            $id,
            'DELETE',
            200,
            $apiKey
        );

        $result = $this->apiClient->execute($transfer);

        return new Webhook($result);
    }
}
