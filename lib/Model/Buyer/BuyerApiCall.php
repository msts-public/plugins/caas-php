<?php
declare(strict_types=1);

namespace Msts\CaaS\Model\Buyer;

use Msts\CaaS\Api\Data\Buyer\BuyerResponseInterface;
use Msts\CaaS\ApiClientInterface;
use Msts\CaaS\Exception\ApiClientException;
use Msts\CaaS\Model\Data\Buyer\BuyerResponse;
use Msts\CaaS\Model\Http\MstsRequest;

class BuyerApiCall
{
    private const METHOD_NAME = 'buyers';

    /**
     * @var ApiClientInterface
     */
    private $apiClient;

    /**
     * @var MstsRequest
     */
    private $mstsRequest;

    public function __construct(
        ApiClientInterface $apiClient,
        MstsRequest $mstsRequest
    ) {
        $this->apiClient = $apiClient;
        $this->mstsRequest = $mstsRequest;
    }

    /**
     * @param string $id
     * @return BuyerResponseInterface
     * @throws ApiClientException
     */
    public function retrieve(string $id): BuyerResponseInterface
    {
        $transfer = $this->mstsRequest->create(
            self::METHOD_NAME,
            [],
            $id
        );
        $result = $this->apiClient->execute($transfer);

        return new BuyerResponse($result);
    }
}
