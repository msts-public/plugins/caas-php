<?php

declare(strict_types=1);

namespace Msts\CaaS\Test\Unit\Model\Credit;

use PHPUnit\Framework\TestCase;
use Msts\CaaS\Model\Buyer\BuyerApiCall;
use Msts\CaaS\Model\Charge\ChargeApiCall;
use Msts\CaaS\Model\Credit\CreditApiCall;
use Msts\CaaS\Model\Preauthorization\PreauthorizationApiCall;
use Msts\CaaS\Model\Webhook\WebhookApiCall;

final class CaaSTest extends TestCase
{
  private $caas;

  protected function setUp(): void
  {
    $caasOptions = new \Msts\CaaS\CaaSOptions();
    $this->caas = new \Msts\CaaS\CaaS('05b527e7-9b01-4875-95f1-9f48397e2112', $caasOptions);
  }

  public function test_can_initialize_credit(): void
  {
    $this->assertInstanceOf(CreditApiCall::class, $this->caas->credit);
  }

  public function test_can_initialize_charge(): void
  {
    $this->assertInstanceOf(ChargeApiCall::class, $this->caas->charge);
  }

  public function test_can_initialize_buyer(): void
  {
    $this->assertInstanceOf(BuyerApiCall::class, $this->caas->buyer);
  }

  public function test_can_initialize_webhooks(): void
  {
    $this->assertInstanceOf(WebhookApiCall::class, $this->caas->webhooks);
  }

  public function test_can_initialize_preauthorization(): void
  {
    $this->assertInstanceOf(PreauthorizationApiCall::class, $this->caas->preauthorization);
  }
}
