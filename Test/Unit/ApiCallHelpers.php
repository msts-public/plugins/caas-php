<?php

function createCaasClient(): Msts\CaaS\CaaS
{
  // /** @var \Psr\Log\LoggerInterface **/
  $logger = new EmptyLogger;
  $caasOptions = new \Msts\CaaS\CaaSOptions();
  $caasOptions->setLogger($logger);
  return new \Msts\CaaS\CaaS('aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee', $caasOptions);
}

class EmptyLogger implements \Psr\Log\LoggerInterface
{
  public function emergency($message, array $context = array()): void
  {
  }
  public function alert($message, array $context = array()): void
  {
  }
  public function critical($message, array $context = array()): void
  {
  }
  public function error($message, array $context = array()): void
  {
  }
  public function warning($message, array $context = array()): void
  {
  }
  public function notice($message, array $context = array()): void
  {
  }
  public function info($message, array $context = array()): void
  {
  }
  public function debug($message, array $context = array()): void
  {
  }
  public function log($level, $message, array $context = array()): void
  {
  }
}
