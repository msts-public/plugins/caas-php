# MSTS CaaS PHP Library

## Installation

### Using Composer

Install the CaaS PHP library with the following command:
```
composer require msts/caas-php
```

## Usage

### Getting Started

The quickest and simplest way to get started is with the following:
```
$caaS = new Msts\CaaS\CaaS('API-KEY');

$webhookList = $caaS->webhooks->list();

var_dump($webhookList);
```

## License
[MPL 2.0](https://www.mozilla.org/en-US/MPL/2.0/)
